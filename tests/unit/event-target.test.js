import test from 'ava';
import { createEvent } from '../../src/event/factory';
import EventTarget from '../../src/event/target';

class Mock extends EventTarget {}
class MockFoo extends EventTarget {}

test('has all the required methods', t => {
  const mock = new Mock();

  t.is(typeof mock.addEventListener, 'function');
  t.is(typeof mock.removeEventListener, 'function');
  t.is(typeof mock.hasEventListeners, 'function');
  t.is(typeof mock.dispatchEvent, 'function');
});

test('adding/removing "message" event listeners works', t => {
  const mock = new Mock();
  const eventObject = createEvent({
    type: 'message'
  });

  const fooListener = event => {
    t.is(event.type, 'message');
  };
  const barListener = event => {
    t.is(event.type, 'message');
  };

  mock.addEventListener('message', fooListener);
  mock.addEventListener('message', barListener);
  mock.dispatchEvent(eventObject);

  mock.removeEventListener('message', fooListener);
  mock.dispatchEvent(eventObject);

  mock.removeEventListener('message', barListener);
  mock.dispatchEvent(eventObject);
});

test('events to different object should not share events', t => {
  const mock = new Mock();
  const mockFoo = new MockFoo();
  const eventObject = createEvent({
    type: 'message'
  });

  const fooListener = event => {
    t.is(event.type, 'message');
  };
  const barListener = event => {
    t.is(event.type, 'message');
  };

  mock.addEventListener('message', fooListener);
  mockFoo.addEventListener('message', barListener);
  mock.dispatchEvent(eventObject);
  mockFoo.dispatchEvent(eventObject);

  mock.removeEventListener('message', fooListener);
  mock.dispatchEvent(eventObject);
  mockFoo.dispatchEvent(eventObject);

  mockFoo.removeEventListener('message', barListener);
  mock.dispatchEvent(eventObject);
  mockFoo.dispatchEvent(eventObject);
});

test('that adding the same function twice for the same event type is only added once', t => {
  const mock = new Mock();
  const fooListener = event => {
    t.is(event.type, 'message');
  };
  const barListener = event => {
    t.is(event.type, 'message');
  };

  mock.addEventListener('message', fooListener);
  mock.addEventListener('message', fooListener);
  mock.addEventListener('message', barListener);

  t.is(mock.listeners.message.length, 2);
});

test('that dispatching an event with multiple data arguments works correctly', t => {
  const mock = new Mock();
  const eventObject = createEvent({
    type: 'message'
  });

  const fooListener = (...data) => {
    t.is(data.length, 3);
    t.is(data[0], 'foo');
    t.is(data[1], 'bar');
    t.is(data[2], 'baz');
  };

  mock.addEventListener('message', fooListener);
  mock.dispatchEvent(eventObject, 'foo', 'bar', 'baz');
});

test('checking for existing "message" event listeners works', t => {
  const mock = new Mock();
  const fooListener = () => {};

  t.is(mock.hasEventListeners('message'), false);

  mock.addEventListener('message', fooListener);
  t.is(mock.hasEventListeners('message'), true);

  mock.removeEventListener('message', fooListener);
  t.is(mock.hasEventListeners('message'), false);
});

test('checking for existing event listeners should not share events', t => {
  const mock = new Mock();

  const fooListener = () => {};
  const barListener = () => {};

  mock.addEventListener('another-message', barListener);
  t.is(mock.hasEventListeners('message'), false);

  mock.removeEventListener('another-message', fooListener);
  mock.addEventListener('message', fooListener);
  t.is(mock.hasEventListeners('another-message'), true);
});
